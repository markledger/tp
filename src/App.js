import React, {useState} from 'react';
import './App.css';
import {Row, Col} from 'react-bootstrap';
import SearchForm from './components/SearchForm';
import ResultsList from './components/ResultsList';
import {ASYNC} from './constants';



function App() {
   
  const [results, setResults] = useState({data:[], status:ASYNC.LOADING});


  return (
    <div className="App container">
      <Row>
        <Col md={9}>

        <SearchForm setResults={setResults} />

        <ResultsList results={results}/>
      
        </Col>
      </Row>
    </div>
  );
}

export default App;

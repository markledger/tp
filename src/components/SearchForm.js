import React, {useState, useEffect} from 'react';
import axios from 'axios'
import queryString from 'query-string';
import {ASYNC} from '../constants';
import {Form} from 'react-bootstrap';
import {debounce} from 'lodash';



const SearchForm = props => {

  const { setResults } = props;
  const [ keyword, setKeyword ] = useState('');
    
	
  useEffect(() => {

		const updateResults = (data, status) => {
			setResults(prevState => ({...prevState, data, status}));
		}

		let cancelToken = axios.CancelToken.source();

		// create a debounced  by 350ms search so we aren't firing off 
		// an API call every keystroke
    const search = debounce(async(pageNum = 1) => {
		
			updateResults([], ASYNC.LOADING);
			

			const qs = queryString.stringify({pageNum, keywords: keyword});
			const url = `/members/oauth/api/job/search.json?${qs}`;

			try{
				const response = await axios.get(url, {cancelToken: cancelToken.token});
				const data = response.data.jobs === null ? [] : response.data.jobs.job;
				updateResults(data, ASYNC.SUCCESS);
			}catch(e){
				updateResults([], ASYNC.ERROR);
			}

		}, 350);


		search();

		//cleanup any outstanding API requests by cleaning up and cancelling them
		return () => cancelToken.cancel("Cancel when unmounted");
	

  }, [keyword, setResults]);

  
  const handleKeywordChange = event => {
    setKeyword(event.target.value);
  }


  return (
		<Form>
			<Form.Group className="mb-3" controlId="formBasicEmail">
				<Form.Label>Job keyword search</Form.Label>
				<Form.Control type="text" placeholder="Job title"value={keyword} onChange={handleKeywordChange} />
			</Form.Group>
		</Form>	
    );


}

export default SearchForm;
import React from 'react';
import ErrorMessage from './ErrorMessage';
import {ASYNC} from '../constants';
import JobItem from './JobItem';

const ResultsList = props => {
	
	const {results} = props;


    if(results.status === ASYNC.SUCCESS && results.data.length === 0){
    	return <ErrorMessage message={"We couldn't find any matching jobs."}/>;
    } 

    if(results.status === ASYNC.ERROR){
    	return <ErrorMessage message={"There was an error contacting the server"} />;
    } 

	return results.data.map(result => <JobItem key={result.job_id} item={result} cols={12}/>);

};


export default ResultsList;
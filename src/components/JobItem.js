import React, {useEffect, useState} from 'react';
import {Card, Button} from 'react-bootstrap';
import {FavouriteStorage} from '../helpers';
import { BsStarFill } from "react-icons/bs";

const JobItem = props => {
	
	const {item, cols} = props;
	const [favouriteStatusChanged, setFavouriteStatusChanged] = useState();

	useEffect(()=>{},[favouriteStatusChanged]);

	const createFavouriteButton = jobId => {
		if(!FavouriteStorage.isAvailable){
			return null;
		}

		if(FavouriteStorage.isFavourited(jobId)){
			return  (<Button variant="danger" className={'col-md-4'} onClick={() => {
				FavouriteStorage.remove(jobId);
				setFavouriteStatusChanged(prevState => !prevState);
			}}><BsStarFill /> Remove from favourites</Button>);
		}

		return <Button variant="success"  className={'col-md-4'} onClick={() => {
			FavouriteStorage.add({job_id: item.job_id, job_title: item.job_title });
			setFavouriteStatusChanged(prevState => !prevState);
		}}><BsStarFill /> Add to favourites</Button>
	}

	const FaveButton = createFavouriteButton(item.job_id);

	return (
		<Card className={`col-md-${cols}`} style={{marginBottom: "10px"}}>
			<Card.Body>
				<Card.Title>{item.job_title}</Card.Title>
				<Card.Text>{item.reference}</Card.Text>
			</Card.Body>
			<Card.Footer>
				{FaveButton}
			</Card.Footer>
		</Card>
	);
}

export default JobItem;
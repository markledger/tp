import {find} from 'lodash';

const FavouriteStorage = {

	add : job => {
		const currentItems = JSON.parse(localStorage.getItem('favourites')) || [];
		const updatedFavourites = JSON.stringify([...currentItems, job]);
		localStorage.setItem('favourites', updatedFavourites);
	},

	remove: jobId => {
		const currentItems = JSON.parse(localStorage.getItem('favourites')) || [];
		const updatedFavourites =  JSON.stringify(currentItems.filter(item => item.job_id !== jobId));
		localStorage.setItem('favourites', updatedFavourites);
	},

	isFavourited: job_id => {
		const currentItems = JSON.parse(localStorage.getItem('favourites')) || [];
		return find(currentItems, {job_id}); 
	},

	purge: () => {
		localStorage.removeItem('favourites');
	},

	getAll: () => JSON.parse(localStorage.getItem('favourites')) || [],

	isAvailable: typeof(Storage) !== "undefined",

}

export {
	FavouriteStorage
};